# Frontend A2O Web App Developer

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 15.2.4.

## Docker build

Run `docker-compose up -d` for build proyect in Docker. Navigate to `http://localhost:4200/`.

## Development server requirements
- Node v18.15.0
- npm 9.5.0
- typescript 4.9.5
## Development server

Run `npm install` in root project for install dependeces. 

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.
