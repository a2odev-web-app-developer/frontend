import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home.component';
import { ProblemOneComponent } from './pages/problem-one/problem-one.component';
import { ProblemTwoComponent } from './pages/problem-two/problem-two.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: 'problem-1',
        component: ProblemOneComponent
      },
      {
        path:'problem-2',
        component: ProblemTwoComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
