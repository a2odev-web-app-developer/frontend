import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { RouterModule } from '@angular/router';
import { ProblemOneComponent } from './pages/problem-one/problem-one.component';
import { ProblemTwoComponent } from './pages/problem-two/problem-two.component';


@NgModule({
  declarations: [
    HomeComponent,
    ProblemOneComponent,
    ProblemTwoComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    RouterModule
  ]
})
export class HomeModule { }
