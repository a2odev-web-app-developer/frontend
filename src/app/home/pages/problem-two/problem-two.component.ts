import { Component, ElementRef, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { ProblemtwoService } from './services/problemtwo.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-problem-two',
  templateUrl: './problem-two.component.html',
  styleUrls: ['./problem-two.component.scss']
})
export class ProblemTwoComponent {

  @ViewChild('input2') public inputProblem?:ElementRef<HTMLTextAreaElement>

  public res = new Subject<number>();
  public error = new Subject<string>();
  constructor(private _problemTwoService: ProblemtwoService) {

  }

  sendProblem(){
    const t = this.inputProblem?.nativeElement.value
    this._problemTwoService.postProblem({t}).subscribe({
      next: (resp) => {
        this.res.next(resp.maxValue)
      },
      error: (err:HttpErrorResponse) => {
        this.error.next(err.error.error)
      }
    })
  }
}
