import { TestBed } from '@angular/core/testing';

import { ProblemtwoService } from './problemtwo.service';

describe('ProblemtwoService', () => {
  let service: ProblemtwoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProblemtwoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
