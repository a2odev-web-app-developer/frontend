import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProblemtwoService {

  constructor(private _http: HttpClient ) { }

  postProblem(data:{}){
    return this._http.post<any>('http://localhost:3000/problem/2',data)
  }
}
