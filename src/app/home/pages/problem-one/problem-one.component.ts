import { Component, ElementRef, ViewChild } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { ProblemOneService } from './services/problem-one.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-problem-one',
  templateUrl: './problem-one.component.html',
  styleUrls: ['./problem-one.component.scss']
})
export class ProblemOneComponent {

  @ViewChild('input1') public inputProblem?:ElementRef<HTMLTextAreaElement>

  public res = new Subject<number>();
  public error = new Subject<string>()

  constructor(private _problemOneService:ProblemOneService) {
  }

  sendProblem() {
    const nativeValue = this.inputProblem?.nativeElement.value
    const splitValue = nativeValue?.split('\n')
    if(!nativeValue || !splitValue || splitValue.length < 2) {
      this.error.next('Does not meet the conditions')
      return
    }
    // const n = splitValue![0].split(' ')[0]
    const [n, k] = splitValue!.shift()!.split(' ').map(i=>+i)
    const [rq, cq] = splitValue!.shift()!.split(' ').map(i=>+i)
    const obstacles = splitValue;
    this._problemOneService.postProblem({n, k, rq, cq, obstacles}).subscribe({
      next: (resp) => {
        this.res.next(resp.result)
      },
      error: (err:HttpErrorResponse) => {
        this.error.next(err.error.error)
      }
    })
  }

}
