import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class ProblemOneService {

  constructor(private _http: HttpClient ) { }

  postProblem(data:{}){
    return this._http.post<any>('http://localhost:3000/problem/1',data)
  }
}
