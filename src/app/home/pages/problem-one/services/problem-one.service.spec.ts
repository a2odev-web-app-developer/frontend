import { TestBed } from '@angular/core/testing';

import { ProblemOneService } from './problem-one.service';

describe('ProblemOneService', () => {
  let service: ProblemOneService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProblemOneService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
