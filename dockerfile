FROM node:18.15.0-alpine3.17 as builder

WORKDIR /app

COPY package.json package-lock.json ./
RUN npm install

COPY . .
RUN npm -v
RUN npm run build

FROM nginx:1.21.0-alpine
COPY nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80

COPY --from=builder /app/dist/frontend /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]
